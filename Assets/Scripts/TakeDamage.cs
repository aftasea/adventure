﻿using UnityEngine;

public class TakeDamage : MonoBehaviour
{
	private Animator animator;

	private void Awake()
	{
		animator = GetComponent<Animator>();
	}

	public void Damage()
	{
		animator.SetTrigger("hit");
	}
}
