﻿using UnityEngine;

public class HeroAttack : MonoBehaviour
{
	public enum AttackType
	{
		None = 0,
		A = 1,
		B = 2
	};

	private Animator animator;

	public bool IsAttackingA
    {
        get {
			return CurrentAttack == AttackType.A
				&& IsAttacking;
		}
    }

	public bool IsAttacking
	{
		get { return animator.GetBool("isAttacking"); }
	}

	private AttackType CurrentAttack
	{
		get { return (AttackType)animator.GetInteger("attackType"); }
	}

	private void Awake()
	{
		animator = GetComponentInChildren<Animator>();
	}

	void Update()
	{
		if (!IsAttacking)
		{
			if (InputManager.WasTriggeredThisFrame(InputManager.Action.AttackA))
				Attack(AttackType.A);
			else if (InputManager.WasTriggeredThisFrame(InputManager.Action.AttackB))
				Attack(AttackType.B);
		}
	}

	private void Attack(AttackType type)
	{
		animator.SetBool("isAttacking", true);
		animator.SetInteger("attackType", (int)type);
	}
}
