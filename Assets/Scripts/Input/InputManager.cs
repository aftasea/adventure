﻿using UnityEngine;
using UnityEngine.InputSystem;

public static class InputManager
{
	public enum Action
	{
		AttackA,
		AttackB,
		HighSpeed,
		LowSpeed
	}

	private static PlayerInput controls;

	static InputManager()
	{
		controls = new PlayerInput();
		controls.Enable();
	}

	public static bool WasTriggeredThisFrame(Action action)
	{
		switch (action)
		{
			case Action.AttackA:
				return controls.Player.AttackA.triggered;
			case Action.AttackB:
				return controls.Player.AttackB.triggered;
		}

		return false;
	}

	public static bool IsPressed(Action action)
	{
		switch (action)
		{
			case Action.HighSpeed:
				return controls.Player.HighSpeed.ReadValue<float>() > 0;
			case Action.LowSpeed:
				return controls.Player.LowSpeed.ReadValue<float>() > 0;
		}

		return false;
	}

	public static Vector2 GetMovement()
	{
		Vector2 movement = controls.Player.Move.ReadValue<Vector2>();
		float deadZone = InputSystem.settings.defaultDeadzoneMin;

		if (Mathf.Abs(movement.x) < deadZone)
			movement.x = 0;
		if (Mathf.Abs(movement.y) < deadZone)
			movement.y = 0;

		return movement.normalized;
	}
}
