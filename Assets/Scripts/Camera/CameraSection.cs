﻿using UnityEngine;
using System;

public class CameraSection : MonoBehaviour
{
    private Collider2D trigger = null;
    CameraSectionManager manager = null;

    void Awake()
    {
        trigger = gameObject.GetComponent<Collider2D>();
        manager = FindObjectOfType<CameraSectionManager>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        manager.ChangeSection(this);
    }
}
