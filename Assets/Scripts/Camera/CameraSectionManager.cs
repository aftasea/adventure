﻿using UnityEngine;

public class CameraSectionManager : MonoBehaviour
{
    [SerializeField]
    private CameraSection initialSection = null;

    private CameraSection currentSection;
    private GameCamera gameCamera;

    public PolygonCollider2D CurrentSectionConfiner
    {
        get
        {
            return currentSection.GetComponent<PolygonCollider2D>();
        }
    }

    void Awake()
    {
        gameCamera = GetComponent<GameCamera>();
        currentSection = initialSection;
        gameCamera.Init(CurrentSectionConfiner);
    }

    public void ChangeSection(CameraSection newSection)
    {
        if (currentSection == newSection)
            return;

        currentSection = newSection;

        gameCamera.StartScrollTransition(newSection.GetComponent<PolygonCollider2D>());
    }
}
