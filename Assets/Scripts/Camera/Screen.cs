﻿using UnityEngine;
using UnityEngine.U2D;

public class Screen
{
    public static float Height
    {
        get { return Camera.main.orthographicSize * 2; }
    }

    public static float Width
    {
        get { return Height * Camera.main.aspect; }
    }

    public static float WidthRefResolution
    {
        get
        {
            PixelPerfectCamera pixelPerfetCamera = Camera.main.GetComponent<PixelPerfectCamera>();
            if (!pixelPerfetCamera)
                Debug.LogWarning("No PixelPerfectCamera found on Camera.main");

            float aspect = (float)pixelPerfetCamera.refResolutionX / (float)pixelPerfetCamera.refResolutionY;

            return Height * aspect;
        }
    }

    public static Vector2 Size
    {
        get { return new Vector2(Width, Height); }
    }

    public static Vector2 SizeRefResolution
    {
        get { return new Vector2(WidthRefResolution, Height); }
    }

    public static float PixelInUnits
    {
        get
        {
            PixelPerfectCamera pixelPerfetCamera = Camera.main.GetComponent<PixelPerfectCamera>();
            if (!pixelPerfetCamera)
                Debug.LogWarning("No PixelPerfectCamera found on Camera.main");
            return 1f / pixelPerfetCamera.assetsPPU;
        }
    }
}
