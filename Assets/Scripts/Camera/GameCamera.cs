﻿using UnityEngine;
using Cinemachine;

public class GameCamera : MonoBehaviour
{
    [SerializeField]
    private HeroMovement hero = null;
    [SerializeField]
    private CinemachineVirtualCamera currentCam;
    [SerializeField]
    private Collider2D currentTransitionConfiner;
    [SerializeField]
    private Collider2D nextTransitionConfiner;

    [SerializeField]
    private CinemachineVirtualCamera mainVCam = null;
    [SerializeField]
    private CinemachineVirtualCamera leavingCam = null;
    [SerializeField]
    private CinemachineVirtualCamera innerCam = null;
    [SerializeField]
    private CinemachineVirtualCamera leavingToFollowingCam = null;
    [SerializeField]
    private CinemachineVirtualCamera innerAfterLeavingCam = null;

    private CinemachineBrain brain;
    private Collider2D destinationConfiner;    
    private CinemachineVirtualCamera transitionCam;
    private CinemachineVirtualCamera destCam;

    Vector2 heroLeavingDirection;

    enum Transition
    {
        None,
        Leaving,
        LeavingToFollowing,
        Inner,
        FollowingOnDestination,
        Cut
    }
    private Transition currentTransitionType = Transition.None;

    private CinemachineConfiner CurrentConfiner
    {
        get { return currentCam.GetComponent<CinemachineConfiner>(); }
    }

    public bool IsInTransition
    {
        get
        {
            return CinemachineCore.Instance.IsLive(transitionCam);
        }
    }

    private void Awake()
    {
        brain = Camera.main.GetComponent<CinemachineBrain>();
    }

    void Update()
    {
        if (!hero.enabled)
        {
            if (!IsInTransition)
            {
                HandleTransitionEnd();
            }
        }
    }

    public void Init(Collider2D confiner)
    {
        currentCam.Priority = 5000;
        CurrentConfiner.m_BoundingShape2D = confiner;
        CurrentConfiner.InvalidatePathCache();
    }

    public void StartScrollTransition(Collider2D destConfiner)
    {
        if (destConfiner == null)
            Debug.LogWarning("Dest.Collider is null");

        hero.enabled = false;

        // saving for setting it after transition
        destinationConfiner = destConfiner;

        // getting the direction the player's leaving the current secion
        heroLeavingDirection = LeavingConfinerDirection();

        // Adjusting hero position to leave origin confiner
        Vector3 heroPosAdjustment = heroLeavingDirection * Screen.PixelInUnits;
        hero.transform.position += heroPosAdjustment;

        currentCam = mainVCam;

        SelectTransition(destConfiner);
    }

    private void SelectTransition(Collider2D destCollider)
    {
        Vector2 currentPos = Camera.main.transform.position;
        Vector2 screen = Screen.Size;
        Vector2 projectedPos = currentPos + (screen * heroLeavingDirection);
        
        // making screen size a bit smaller to prevent floating point precision issues on collision detection
        float screenEpsilon = 0.01f;
        Vector2 reducedScreen = new Vector2(screen.x - screenEpsilon, screen.y - screenEpsilon);
        Bounds projection = new Bounds(projectedPos, reducedScreen);

        // checking if the projected screen corners are inside the destination collider
        Vector2 projectionTopLeft = new Vector2(projection.min.x, projection.max.y);
        bool isTopLeftConfined = destCollider.bounds.Contains(projectionTopLeft);

        Vector2 projectionTopRight = projection.max;
        bool isTopRightConfined = destCollider.bounds.Contains(projectionTopRight);

        Vector2 projectionBottomLeft = projection.min;
        bool isBottomLeftConfined = destCollider.bounds.Contains(projectionBottomLeft);

        Vector2 projectionBottomRight = new Vector2(projection.max.x, projection.min.y);
        bool isBottomRightConfined = destCollider.bounds.Contains(projectionBottomRight);

        // if all four projected corners are contained into destination confiner -> do a normal transition
        if (isTopLeftConfined && isTopRightConfined && isBottomLeftConfined && isBottomRightConfined)
        {
            // Check to see if cam will arrive to destination after transition
            Vector2 heroPos = hero.transform.position;
            Vector2 distanceToHero = heroPos - projectedPos;

            // If transition is VERTICAL
            if (heroLeavingDirection == Vector2.up || heroLeavingDirection == Vector2.down)
            {
                if (Mathf.Abs(distanceToHero.x) < (4f * Screen.PixelInUnits))
                {
                    StartLeavingTransition(projectedPos);
                    return;
                }

                Vector2 finalPos = new Vector2(projectedPos.x + distanceToHero.x, projectedPos.y);
                Bounds finalProjection = new Bounds(finalPos, reducedScreen);

                // Will projected screen be inside destination confiner bounds?
                if (destinationConfiner.bounds.Contains(finalProjection.min) && destinationConfiner.bounds.Contains(finalProjection.max))
                {
                    // Cam will NOT arrived to destination yet. Will need an extra INNER transition on destination
                    StartLeavingIntermediateTransition(projectedPos);
                    return;
                }
            }

            // If transition is HORIZONTAL
            else if (heroLeavingDirection == Vector2.right || heroLeavingDirection == Vector2.left)
            {
                if (Mathf.Abs(distanceToHero.y) < (4f * Screen.PixelInUnits))
                {
                    StartLeavingTransition(projectedPos);
                    return;
                }

                Vector2 finalPos = new Vector2(projectedPos.x, projectedPos.y + distanceToHero.y);
                Bounds finalProjection = new Bounds(finalPos, reducedScreen);

                // Will projected screen be inside destination confiner bounds?
                if (destinationConfiner.bounds.Contains(finalProjection.min) && destinationConfiner.bounds.Contains(finalProjection.max))
                {
                    // Cam will NOT arrived to destination yet. Will need an extra INNER transition on destination
                    StartLeavingIntermediateTransition(projectedPos);
                    return;
                }
            }

            StartLeavingTransition(projectedPos);   
            return;
        }

        // if not, check which corners are not contained in destination conf. to do an internal perpendicular transition

        // if leaving up or down, then the transition should be horizontal
        // Note: Here, Vector2 == operator is doing an Approx comparisson to avoid float presicion errors
        if (heroLeavingDirection == Vector2.down || heroLeavingDirection == Vector2.up)
        {
            // left border is confined (when right is not)
            if (isTopLeftConfined && isBottomLeftConfined) // then, move to left
            {
                float currentCamRight = currentPos.x + (screen.x * 0.5f);
                float destColliderRight = destCollider.bounds.max.x;
                float destX = currentPos.x - (currentCamRight - destColliderRight);
                Vector2 perpendicularTransitionDestPos = new Vector2(destX, currentPos.y);

                // intermediate transition to the left (inside current confiner)
                StartInnerTransition(perpendicularTransitionDestPos);
                return;
            }
            // right border is confined (when left is not)
            else if (isTopRightConfined && isBottomRightConfined) // then, move to right
            {
                float currentCamLeft = currentPos.x - (screen.x * 0.5f);
                float destColliderLeft = destCollider.bounds.min.x;
                float destX = currentPos.x + (destColliderLeft - currentCamLeft);
                Vector2 perpendicularTransitionDestPos = new Vector2(destX, currentPos.y);

                // intermediate transition to the right (inside current confiner)
                StartInnerTransition(perpendicularTransitionDestPos);
                return;
            }
        }

        // continue handling cases for VERTICAL inner transition (when leaving to the left or right)
        if (heroLeavingDirection == Vector2.left || heroLeavingDirection == Vector2.right)
        {
            // top border is confined (when bottom is not)
            if (isTopLeftConfined && isTopRightConfined) // then, move up
            {
                float currentCamBottom = currentPos.y + (screen.y * 0.5f);
                float destColliderBottom = destCollider.bounds.min.y;
                float destY = currentPos.y + (destColliderBottom - currentCamBottom);
                Vector2 perpendicularTransitionDestPos = new Vector2(currentPos.x, destY);

                // intermediate transition up (inside current confiner)
                StartInnerTransition(perpendicularTransitionDestPos);
                return;
            }
            // bottom border is confined (when top is not)
            else if (isBottomLeftConfined && isBottomRightConfined) // then, move down
            {
                float currentCamTop = currentPos.y + (screen.y * 0.5f);
                float destColliderTop = destCollider.bounds.max.y;
                float destY = currentPos.y - (currentCamTop - destColliderTop);
                Vector2 perpendicularTransitionDestPos = new Vector2(currentPos.x, destY);

                // intermediate transition downwards (inside current confiner)
                StartInnerTransition(perpendicularTransitionDestPos);
                return;
            }
        }

        Debug.LogWarning("Unexpected case not handled by SelectTransition. Reposition cam without transition.");
        StartCutTransition();
    }

    /**************************************************/

    private Vector2 LeavingConfinerDirection()
    {
        Vector2 currentConfLeftBottom = CurrentConfiner.m_BoundingShape2D.bounds.min;
        Vector2 currentConfRightTop = CurrentConfiner.m_BoundingShape2D.bounds.max;

        Vector2 destConfLeftBottom = destinationConfiner.bounds.min;
        Vector2 destConfRightTop = destinationConfiner.bounds.max;

        if (destConfLeftBottom.x >= currentConfRightTop.x)
            return Vector2.right;

        if (destConfRightTop.x <= currentConfLeftBottom.x)
            return Vector2.left;

        if (destConfLeftBottom.y >= currentConfRightTop.y)
            return Vector2.up;

        if (destConfRightTop.y <= currentConfLeftBottom.y)
            return Vector2.down;

        // fallback
        Debug.LogWarning("Leaving direction not found o_O");
        return Vector2.zero;
    }
    
    private void SwapTransitionColliders()
    {
        Collider2D tmp = currentTransitionConfiner;
        currentTransitionConfiner = nextTransitionConfiner;
        nextTransitionConfiner = tmp;
    }

    private void StartLeavingTransition(Vector2 transitionDestinationPosition)
    {
        destCam = leavingCam;
        transitionCam = currentCam;

        currentCam.Priority = 10;

        Collider2D transitionConfinerCollider = currentTransitionConfiner;
        transitionConfinerCollider.transform.position = transitionDestinationPosition;
        SetDestinationCamConfinerBounds(currentTransitionConfiner);

        destCam.Priority = 150;

        currentTransitionType = Transition.Leaving;
    }

    private void StartLeavingIntermediateTransition(Vector2 transitionDestinationPosition)
    {
        destCam = leavingToFollowingCam;
        transitionCam = currentCam;

        currentCam.Priority = 10;

        Collider2D transitionConfinerCollider = currentTransitionConfiner;
        transitionConfinerCollider.transform.position = transitionDestinationPosition;
        SetDestinationCamConfinerBounds(currentTransitionConfiner);

        destCam.Priority = 150;

        currentTransitionType = Transition.LeavingToFollowing;
    }

    private void StartInnerTransition(Vector2 transitionDestinationPosition)
    {
        destCam = innerCam;
        transitionCam = currentCam;

        currentCam.Priority = 10;

        Collider2D transitionConfinerCollider = currentTransitionConfiner;
        transitionConfinerCollider.transform.position = transitionDestinationPosition;
        SetDestinationCamConfinerBounds(currentTransitionConfiner);

        // swapping transition confiners
        SwapTransitionColliders();

        destCam.Priority = 150;

        currentTransitionType = Transition.Inner;
    }

    private void StartFollowingOnDestinationTransition()
    {
        currentCam = destCam;
        destCam = innerAfterLeavingCam;
        transitionCam = currentCam;

        currentCam.Priority = 10;

        SetDestinationCamConfinerBounds(destinationConfiner); // final dest zone confiner

        destCam.Priority = 150;

        currentTransitionType = Transition.FollowingOnDestination;
    }

    private void StartCutTransition()
    {
        currentCam = destCam;
        currentCam.Priority = 10;
        transitionCam = currentCam;

        // adjusting mainVCam position for 'cut' transition
        mainVCam.gameObject.SetActive(false);
        brain.enabled = false;
        mainVCam.transform.position = Camera.main.transform.position;
        mainVCam.gameObject.SetActive(true);
        brain.enabled = true;

        destCam = mainVCam;
        SetDestinationCamConfinerBounds(destinationConfiner);
        destCam.Priority = 150;

        currentTransitionType = Transition.Cut;
    }

    private void HandleTransitionEnd()
    {
        switch (currentTransitionType)
        {
            case Transition.Inner:
                currentCam = destCam;
                SelectTransition(destinationConfiner);
                break;
            case Transition.LeavingToFollowing:
                StartFollowingOnDestinationTransition();
                break;
            case Transition.Cut:
                // On destination already!
                FinishTransition();
                break;
            case Transition.Leaving:
            case Transition.FollowingOnDestination:
                StartCutTransition();
                break;
            default:
                Debug.LogWarning("Unhandled transition end for: " + currentTransitionType);
                StartCutTransition();
                break;
        }
    }

    private void FinishTransition()
    {
        currentCam.Priority = 0;
        currentCam = mainVCam;
        currentCam.Priority = 150;
        destCam = null;
        transitionCam = null;
        currentTransitionType = Transition.None;
        RestoreHero();
    }

    private void RestoreHero()
    {
        hero.enabled = true;
    }

    private void SetDestinationCamConfinerBounds(Collider2D collider)
    {
        CinemachineConfiner destConfiner = destCam.GetComponent<CinemachineConfiner>();
        destConfiner.m_BoundingShape2D = collider;
        destConfiner.InvalidatePathCache();
    }

    private void SetCamConfinerBounds(CinemachineVirtualCamera cam, Collider2D collider)
    {
        CinemachineConfiner destConfiner = cam.GetComponent<CinemachineConfiner>();
        destConfiner.m_BoundingShape2D = collider;
        destConfiner.InvalidatePathCache();
    }
}
