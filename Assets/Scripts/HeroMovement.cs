﻿using UnityEngine;

public class HeroMovement : MonoBehaviour
{
    [SerializeField]
    private float speedInSeconds = 1f;

    private Rigidbody2D rigidBody;
    private Vector2 facingDirection = Vector2.down;
    private Vector2 newDirection;
    private HeroAttack heroAttack;

    // debug
    [SerializeField]
    private float highSpeedFactor = 3f;
    [SerializeField]
    private float lowSpeedFactor = 0.3f;

    public Vector2 Direction
    {
        get { return facingDirection; }
    }

    public bool IsMoving
    {
        get;
        private set;
    }

    private bool CanMove
    {
        get { return !heroAttack.IsAttackingA; }
    }

    void Awake()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        heroAttack = GetComponent<HeroAttack>();
        IsMoving = false;
    }

    private void OnDisable()
    {
        rigidBody.velocity = Vector2.zero;
    }

    void FixedUpdate ()
    {
        UpdateNewDirection();

        if (newDirection != Vector2.zero)
        {
            UpdateFacingDirection();
            IsMoving = true;
        }
        else
        {
            IsMoving = false;
        }

        UpdateVelocity();
    }

	private void UpdateNewDirection()
    {
        newDirection = Vector2.zero;
        if (CanMove)
            UpdateNewDirectionFromInput();
    }

    private void UpdateNewDirectionFromInput()
    {
		newDirection = InputManager.GetMovement();
	}

	private void UpdateFacingDirection()
    {
        if (facingDirection != newDirection)
        {
            const float epsilon = 0.01f;
            // this (epsilon * facingDirection) is used to keep the last facing direction when moving diagonally
            facingDirection = (epsilon * facingDirection) + newDirection;
            facingDirection.Normalize();
        }
    }

    private void UpdateVelocity()
    {
        rigidBody.velocity = newDirection * speedInSeconds;

		if (InputManager.IsPressed(InputManager.Action.HighSpeed))
			rigidBody.velocity *= highSpeedFactor;
		if (InputManager.IsPressed (InputManager.Action.LowSpeed))
			rigidBody.velocity *= lowSpeedFactor;
	}
}
