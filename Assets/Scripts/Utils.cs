﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utils
{
	public static bool Approximately(float a, float b)
	{
		const float epsilon = 0.01f;
		return Mathf.Abs(a - b) < epsilon;
	}
}
