﻿using UnityEngine;

public class HeroAnimationController : MonoBehaviour
{
    private Animator animator;
    private HeroMovement hero;

	void Awake ()
    {
        animator = GetComponentInChildren<Animator>();
        hero = GetComponent<HeroMovement>();
	}

	void Update()
	{
		animator.SetFloat("directionX", hero.Direction.x);
		animator.SetFloat("directionY", hero.Direction.y);
		animator.SetInteger("dirX", HeroFacingDirection().x);
		animator.SetInteger("dirY", HeroFacingDirection().y);
		animator.SetBool("isMoving", hero.IsMoving);
    }

	private Vector2Int HeroFacingDirection()
	{
		// comparing angle between hero direction and some axis (e.g. +X)
		float dotProduct = Vector2.Dot(hero.Direction, Vector2.right);

		// 45 degrees = PI/4 rad
		// proyected to x or y axis is = sin(PI/4)
		float limit = Mathf.Sin(Mathf.PI/4);

		// angle is inside 90 deg around +X axis (between -45 to 45)
		if (dotProduct >= limit)
			return Vector2Int.right;

		// angle is inside 90 deg around Y axis (check if + or  -)
		if (dotProduct >= -limit)
			return hero.Direction.y >= 0f ? Vector2Int.up : Vector2Int.down;

		// else, dotProduct >= -1f
		return Vector2Int.left;
	}
}
