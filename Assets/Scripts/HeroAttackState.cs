﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroAttackState : StateMachineBehaviour
{
	private const string isAttackingKey = "isAttacking";
	private const string attackTypeKey = "attackType";

	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		animator.SetBool(isAttackingKey, false);
		animator.SetInteger(attackTypeKey, (int)HeroAttack.AttackType.None);
	}

}
