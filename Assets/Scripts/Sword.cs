﻿using UnityEngine;

public class Sword : MonoBehaviour
{
	void OnTriggerEnter2D(Collider2D other)
	{
		Debug.Log("SwordCollision. " + other.name);
		if (other.tag == "Enemy")
		{
			TakeDamage enemy = other.GetComponent<TakeDamage>();
			enemy?.Damage();
		}
	}
}
